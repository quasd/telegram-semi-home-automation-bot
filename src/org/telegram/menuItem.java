package org.telegram;

public class menuItem {
	
	String name;
	String command;
	String type;
	String description;
	
	public menuItem(String newName, String newCommand,String newType, String newDescription) {
		name = newName;
		command = newCommand;
		type = newType;
		description = newDescription;
	}
	public menuItem(String newName) {
		name = newName;
	}
}
