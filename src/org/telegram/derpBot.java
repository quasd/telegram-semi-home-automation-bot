package org.telegram;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.logging.BotLogger;

public class derpBot extends TelegramLongPollingBot
{
  BotConfig bot = new BotConfig();
  static List<topItem> topMenuItems = new ArrayList<>();
  
  public static void main(String[] args)
  {
	// Main menu
	topItem menu = new topItem("menu",null);
	
	// Create PC submenu
	topItem pc = new topItem("PC",null);
	ArrayList<menuItem> pcFirstRow = new ArrayList<menuItem>();
	ArrayList<menuItem> pcSecondRow = new ArrayList<menuItem>();
	ArrayList<menuItem> pcThirdRow = new ArrayList<menuItem>();
	ArrayList<menuItem> pcForthRow = new ArrayList<menuItem>();
	
	menuItem mute = new menuItem("Mute audio","amixer set Master mute","ssh","muted audio");
	menuItem unMute = new menuItem("Unmute audio","amixer set Master unmute","ssh","unmuted audio");
	pcFirstRow.add(mute);
	pcFirstRow.add(unMute);
	
	menuItem volUp = new menuItem("Increase volume","amixer set Master unmute;amixer set Master 5%+","ssh","increase volume");
	menuItem volDown = new menuItem("Decrease volume","amixer set Master unmute;amixer set Master 5%-","ssh","decreased volume");
	pcForthRow.add(volUp);
	pcForthRow.add(volDown);
	
	menuItem displayOff = new menuItem("Display off","DISPLAY=:0.0 xset dpms force suspend","ssh","turned off the dispaly");
	pcSecondRow.add(displayOff);
	
	menuItem reboot = new menuItem("Reboot","sudo reboot","ssh","pc rebooted");
	menuItem shutdown = new menuItem("Shutdown","sudo poweroff","ssh","pc turned off");
	menuItem pcwol = new menuItem("WOL (TODO)","TODO","ssh","pc WOL");
	pcThirdRow.add(reboot);
	pcThirdRow.add(shutdown);
	pcThirdRow.add(pcwol);
	
	pc.addItem(pcFirstRow);
	pc.addItem(pcForthRow);
	pc.addItem(pcSecondRow);
	pc.addItem(pcThirdRow);
	
	// Create Vacuum Submenu
	topItem vacuum = new topItem("Vacuum","VACUUM");
	ArrayList<menuItem> vacuumFirstRow = new ArrayList<menuItem>();
	ArrayList<menuItem> vacuumSecondRow = new ArrayList<menuItem>();
	ArrayList<menuItem> vacuumThirdRow = new ArrayList<menuItem>();
	ArrayList<menuItem> vacuumForthRow = new ArrayList<menuItem>();
	
	menuItem vacuumStart = new menuItem("Start/Pause","START_PAUSE","irsend","Starting / Pausing VACUUM cleaner");
	vacuumFirstRow.add(vacuumStart);
	
	menuItem vacuumGoHome = new menuItem("Go Home","GO_HOME","irsend","Returning to home! (if somehow reached cleaner)");
	vacuumSecondRow.add(vacuumGoHome);
	
	menuItem vacuumMaxPower = new menuItem("MAX power","MAX_POWER","irsend","Switching to max power.");
	vacuumThirdRow.add(vacuumMaxPower);
	
	menuItem vacuumSpotClean = new menuItem("Clean spot","CLEAN_SPOT","irsend","Cleaning a spot!");
	vacuumForthRow.add(vacuumSpotClean);
	
	vacuum.addItem(vacuumFirstRow);
	vacuum.addItem(vacuumSecondRow);
	vacuum.addItem(vacuumForthRow);
	vacuum.addItem(vacuumThirdRow);
	
	// Create AC Submenu
	topItem ac = new topItem("AC","AC");
	ArrayList<menuItem> acFirstRow = new ArrayList<menuItem>();
	ArrayList<menuItem> acSecondRow = new ArrayList<menuItem>();
	ArrayList<menuItem> acThirdRow = new ArrayList<menuItem>();
	
	menuItem acCooling = new menuItem("cooling 25°C","COOLING_25C","irsend","COOLING down the appartment to 25°C");
	menuItem acCoolingDir = new menuItem("cooling 24°C - Change Direction","COOLING_24C_DIRECTION","irsend","Changing direction (cooling 24°C)");
	acFirstRow.add(acCooling);
	acFirstRow.add(acCoolingDir);
	
	menuItem acHeating = new menuItem("heating 20°C","HEATING_20C","irsend","WARMING up the appartment to 20°C");
	acSecondRow.add(acHeating);
	
	menuItem acOff = new menuItem("off","OFF","irsend","Finally turned off the AC");
	acThirdRow.add(acOff);
	
	
	ac.addItem(acFirstRow);
	ac.addItem(acSecondRow);
	ac.addItem(acThirdRow);
	
	// Create Light Submenu
	topItem light = new topItem("Light","panasonic-hk9487");
	ArrayList<menuItem> lightFirstRow = new ArrayList<menuItem>();
	ArrayList<menuItem> lightSecondRow = new ArrayList<menuItem>();
	ArrayList<menuItem> lightThirdRow = new ArrayList<menuItem>();
	ArrayList<menuItem> lightForthRow = new ArrayList<menuItem>();
	ArrayList<menuItem> lightFifthRow = new ArrayList<menuItem>();
	
	menuItem lightOn = new menuItem("on","pwr","irsend","Turned lights on");
	menuItem lightOff = new menuItem("off","pwrOff","irsend","turned lights off");
	lightFirstRow.add(lightOn);
	lightFirstRow.add(lightOff);
	
	menuItem lightMax = new menuItem("max power","maxPwr","irsend","set light to maximum brightness");
	menuItem lightNightMode = new menuItem("sleep mode","nightMode","irsend","sleep SLEEP sleeep");
	lightSecondRow.add(lightMax);
	lightSecondRow.add(lightNightMode);
	
	menuItem lightIncBrightness = new menuItem("increase brightness","incBrightness","irsend","increasing brightness");
	menuItem lightDecBrightness = new menuItem("decrease brightness","decBrightness","irsend","decreasing brightness");
	lightThirdRow.add(lightIncBrightness);
	lightThirdRow.add(lightDecBrightness);
	
	menuItem lightIncColor = new menuItem("increase color temp","incColor","irsend","increasing color temperature");
	menuItem lightDecColor = new menuItem("decrease colo tempr","decColor","irsend","Decreasing color temperature");
	lightForthRow.add(lightIncColor);
	lightForthRow.add(lightDecColor);
	
	menuItem lightSleepTimer = new menuItem("30 minute timer","30minTimer","irsend","Starting 30 min sleep timer...");
	lightFifthRow.add(lightSleepTimer);
	
	light.addItem(lightFirstRow);
	light.addItem(lightSecondRow);
	light.addItem(lightThirdRow);
	light.addItem(lightForthRow);
	light.addItem(lightFifthRow);
	
	// Add created menus to list
	topMenuItems.add(pc);
	topMenuItems.add(ac);
	topMenuItems.add(vacuum);
	topMenuItems.add(light);
	topMenuItems.add(menu);
	

    System.out.println("Starting " + BotConfig.BOT_USERNAME);
    String LOGTAG = BotConfig.BOT_USERNAME + " error";
    
    ApiContextInitializer.init();
    TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
    try{
      telegramBotsApi.registerBot(new derpBot());
    }
    catch (TelegramApiException e)
    {
      BotLogger.error(LOGTAG, e);
    }
  }
  
  public String getBotUsername()
  {
    return BotConfig.BOT_USERNAME;
  }
  
  public ReplyKeyboardMarkup getKeyboard(topItem topitem)
  {
    ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
    replyKeyboardMarkup.setSelective(Boolean.valueOf(true));
    replyKeyboardMarkup.setResizeKeyboard(Boolean.valueOf(true));
    replyKeyboardMarkup.setOneTimeKeyboard(Boolean.valueOf(false));
    List<KeyboardRow> keyboard = new ArrayList<KeyboardRow>();
    
    if(topitem.name.equals("menu")) {
    	// Create the main menu
		for (topItem singleTopItem : topMenuItems) {
			KeyboardRow keyboarRow = new KeyboardRow();
			// add everything but the "menu"
			if (! singleTopItem.name.equalsIgnoreCase("menu")) {
    			keyboarRow.add(singleTopItem.name);
			}
			// Add the created row
			keyboard.add(keyboarRow);
		}
    }else {
    	// Create other than menu menus
        for (ArrayList<menuItem> row : topitem.itemsRows) {
        	KeyboardRow keyboarRow = new KeyboardRow();
        	for (menuItem column : row) {
    			// all but menu row need prefix
    			keyboarRow.add(topitem.getName()+ " "+ column.name);
        	}
        	keyboard.add(keyboarRow);
        }
        // Add the menu key last
        KeyboardRow menuRow = new KeyboardRow();
        menuRow.add("menu");
        keyboard.add(menuRow);

    }


    replyKeyboardMarkup.setKeyboard(keyboard);
    replyKeyboardMarkup.setOneTimeKeyboard(Boolean.valueOf(false));
    return replyKeyboardMarkup;
  }
  
  public void runCommandLirc(String command, String lircProfile, Boolean local)
  {
	String newCommand = "irsend"+ " " + "SEND_ONCE"+ " " +lircProfile + " " + command; 
    try{
      if (local)
      {
    	// Run the command eg. irsend SEND_ONCE ac 
        ProcessBuilder pb = new ProcessBuilder("irsend", "SEND_ONCE", lircProfile,command);
        pb.redirectOutput(ProcessBuilder.Redirect.INHERIT);
        pb.redirectError(ProcessBuilder.Redirect.INHERIT);
        pb.start(); //Process localProcess = pb.start();
      }
      else{
    	System.out.println( "ssh " + BotConfig.remoteServerIP + " " + newCommand);
        Runtime.getRuntime().exec(new String[] { "ssh", BotConfig.remoteServerIP, newCommand });
      }
    }
    catch (IOException e)
    {
      e.printStackTrace();
      System.out.print("crashed");
    }
  }
  
  public void runCommandSsh(String command){
    try{
      String newCommand = "ssh -t " + BotConfig.sshHost + " " + command;
      System.out.print(newCommand);
      Runtime.getRuntime().exec( newCommand );
    }
    catch (IOException e)
    {
      e.printStackTrace();
      System.out.print("crashed");
    }
  }
  
  public void runCommand(String type, String command, String lircProfile){
	if (type.equals("ssh")) {
		System.out.print("Running ssh command");
		runCommandSsh(command);
	}else if(type.equals("irsend")) {
		System.out.print("Running lirc command");
		runCommandLirc(command, lircProfile, BotConfig.runLocaly);
	}
  }
  
  @Override
  public void onUpdateReceived(Update update)
  {
    System.out.println("Got command");
    if (update.hasMessage())
    {
      Message message = update.getMessage();
      
      Boolean noChange = true;
      if (BotConfig.allowedUsers.contains(message.getChatId()))
      {
        if (message.hasText())
        {
          SendMessage sendMessageRequest = new SendMessage();
          String text = message.getText().toLowerCase();
          ReplyKeyboardMarkup replyKeyboardMarkup;
          Boolean noMatch = true;
          // Loop through top items
          for (topItem topitem :topMenuItems) {
        	  // if the client sent text equals to top item name
        	  if (text.equals(topitem.name.toLowerCase())){
        		  //Show the correct keyboard
        		  System.out.println("adding keyboard " +topitem.name.toLowerCase());
        		  replyKeyboardMarkup = getKeyboard(topitem);
            	  sendMessageRequest.setReplyMarkup(replyKeyboardMarkup);
            	  sendMessageRequest.setText("Select action:");
            	  // Since we only are showing possible actions, channel doesn't need to be notified
            	  noChange = true;
            	  // We already found what we want to do, no need to test sub menus commands
            	  noMatch = false;
            	  break;
        	  }
          }      
          if (noMatch) {
        	  // Loop through top items
        	  for (topItem item :topMenuItems) {
        		  // If we can find top item and command after it that matches message from client
        		  if (text.contains(item.name.toLowerCase() + " ")){
        			  String command = message.getText().replace(item.name + " ", "");
        			  // Loop through rows in item
        			  for (ArrayList<menuItem> singleRow : item.itemsRows) {
        				  // Loop through columns in item
        				  for (menuItem column : singleRow) {
            				  // If command from client matches menuItem
            				  if(command.equalsIgnoreCase(column.name)) {
            					  // Execute command
            					  runCommand(column.type, column.command, item.lircName);
            					  // Send message to the client
    							  sendMessageRequest.setText(column.description);
    							  // We want channel to notified so
    							  noChange = false;
            				  }
        				  }

        			  }
        		  }
        	  }
          }
          // Set the reply target to where we got the message 
          sendMessageRequest.setChatId(message.getChatId().toString());
          try {
        	System.out.println("Sending reply :" + noChange);
        	// Send reply to channel it came from 
           	execute(sendMessageRequest);
            if ( noChange ) {
              return;
            }else {
            	// Make the message username : <Old message>
                sendMessageRequest.setText(BotConfig.users.get(BotConfig.allowedUsers.indexOf(Long.valueOf(sendMessageRequest.getChatId()))) + ":" + sendMessageRequest.getText());
                // Set output to first allowed user. (This should be set to output channel)
                sendMessageRequest.setChatId(BotConfig.allowedUsers.get(0));
                // Send notification to the output channel
                execute(sendMessageRequest);
                System.out.println("Reply sent");
            }
          }
          catch (TelegramApiException localTelegramApiException) {}
        }
      }
      else
      {
        SendMessage sendMessageRequest = new SendMessage();
        sendMessageRequest.setChatId(message.getChatId());
        sendMessageRequest.setText("Sorry but you don't have access to this bot. :(");
        try{
        	// Send error message to whoever sent a request. 
           execute(sendMessageRequest);
           // Print debug 
           System.out.print(
              "Failed login from " + 
              message.getChatId().toString() + " " + 
              message.getChat().getUserName() + " " + 
              message.getChat().getFirstName() + " " + 
              message.getChat().getLastName() + " ");
         }
        catch (TelegramApiException localTelegramApiException1) {}
      }
    }
  }
  
  public String getBotToken()
  {
    return BotConfig.BOT_TOKEN;
  }
}
