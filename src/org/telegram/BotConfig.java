package org.telegram;

import java.util.Arrays;
import java.util.List;

public class BotConfig
{
  // Telegram Bot username 
  public static final String BOT_USERNAME = "<BOT USERNAME>";
  // Bot token
  public static final String BOT_TOKEN = "<BOT TOKEN>";
  
  
  // List of users who are allowed to use this bot eg. 5353626L. All actions will send extra notification to first user of this list.
  // First 'user' should be set as channel used for notifications of all actions bot makes. 
  public static final List<Long> allowedUsers = Arrays.asList(new Long[] { -<Chat ID>L, <User ID>L, <User ID>L });
  // List of usernames for said users
  public static final List<String> users = Arrays.asList(new String[] { "<Name of CHAT>", "<display name for user>","<display name for user>" });
  
  
  // mac address to be used when wakin up PC (TODO, not implemented yet)
  public static final String WAKE_MAC = "60:81:c2:45:9f:1b";
  
  
  // Should we run lirc locally or over ssh?
  public static final Boolean runLocaly = true;
  // If above is false, below username@ip will be used (pubkey auth necessary)
  public static final String  remoteServerIP = "quasd@192.168.1.151"; 
  
  
  // User and ip address for pc
  public static final String sshHost = "quasd@192.168.1.152";
  
}
