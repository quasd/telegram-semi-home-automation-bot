# Telegram semi home automation bot

 - Control computer, appliances via ssh/lirc from Telegram bot
 - Uses telegram keyboard markup for interaction

## Main menu example

![alt text](1.png)

## Ac menu example

![alt text](2.png)

## Pc menu example

![alt text](3.png)


# Configuration

Most of the settings are in BotConfig.java file. Adjust all the necessary parameters.

# Run as systemctl user service

Copy example service file to following location and modify path used on ExecStart.
~/.config/systemd/user/telBot.service

```
systemctl --user daemon-reload
systemctl --user start telBot.service
systemctl --user enable telBot.service
```
